# TUTORIEL RADICALE 1.1 
![RADICALE logo](https://s1.qwant.com/thumbr/0x0/0/9/5b40e9013a3c970aa2ad4693da9b4a/b_1_q_0_p_0.jpg?u=http%3A%2F%2Fwww.pihomeserver.fr%2Fwp-content%2Fuploads%2F2013%2F08%2Fradicale.png&q=0&b=1&p=0&a=1)

<http://radicale.org>

Dans ce tutoriel, nous allons voir l'installation et le paramétrage de RADICALE, un serveur Carddav, Caldav dont la configuration se fait entièrement en ligne de commande et donc sans interface web. (comme owncloud par exemple). Il est très léger du fait notamment de l'absence de bases de données, les données étant stockées « à plat » dans des fichiers .ics pour les agendas et .vcf pour les contacts.

**​Ce tutoriel n'a pas la prétention de vous fournir l'ensemble des configurations possibles**. Il vous propose un exemple de configuration tout en vous donnant la logique de sa mise en place.

Avant d'installer RADICALE, il est nécessaire de configurer Apache2, d'ouvrir le port 5232 (par défaut pour une configuration standard) dans le pare-feu et de créer les certificats permettant de configurer la sécurité dans les échanges avec le serveur. Je vous conseille de respecter les étapes de la configuration.



## A°) CONFIGURATION STANDARD (Recommandée pour débuter et comprendre son fonctionnement) :

​Radicale gère l'authentification des utilisateurs ainsi que les droits


Radicale gère l'authentification des utilisateurs ainsi que les droits d'accès aux bibliothèques. Il est nécessaire d'ouvrir le port consacré à Radicale dans le Pare-Feu.	

### 1°) Ouvrir le port 5232 (port par défaut) dans le pare-feu
Les règles du parefeu sont affichés avec la commande suivante :

	sudo iptables -L

On peux vérifier de suite si le **port https** est bien activé. On doit y trouver au moins les paramètres suvant dans INPUT :

	Chain INPUT (policy ACCEPT)
	target     prot opt source               destination         
	ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:http
	ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:https
	ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:ssh
	ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:5232

Le port https doit être ouvert. Pour ajouter une règle au parefeu et ici pour ajouter le port 5232

	sudo iptables -A INPUT -p tcp -m tcp --dport 5232 -j ACCEPT 

la règle du parefeu doit être sauvegardée pour être chargée au demarrage. Il faut installer **le paquet iptables-persistent**. Je vous renvoie ici vers la documentation, en fonction de votre distribution.

La commande suivante permet d'afficher les connexions internet actives.
​	
	netstat -tulpen

### 2°) Générer le certificat SSL qui sera stocké dans /etc/apache2/ssl (créer le dossier ssl s'il n'y est pas)

#### Étape 1
Si ce n'est pas déjà fait, installer apache2 et openssl :

	sudo apt-get install -y apache2 openssl

Créer le répertoire qui recevra les fichiers .key et .crt.

	sudo mkdir /etc/apache2/ssl

Générer la clé RSA en 2048, générer le "CSR" et lui enlever la passphrase :

	sudo openssl genrsa -des3 -out apache.key 2048
	sudo openssl req -new -key apache.key -out apache.csr
	sudo cp apache.key  apache.key.orig
	sudo openssl rsa -in apache.key.orig -out apache.key

#### Étape 2
Pour ajouter le flag CA:true, il faut créer un fichier, disons **openssl.cnf** avec comme contenu :

	basicConstraints=CA:TRUE

#### Étape 3
Puis générer le certificat :

	sudo openssl x509 -req -days 3650 -in apache.csr -signkey apache.key -out apache.crt -extfile openssl.cnf

> Let's go over exactly what this means.

>* openssl: This is the basic command line tool provided by OpenSSL to create and manage certificates, keys, signing requests, etc.
* req: This specifies a subcommand for X.509 certificate signing request (CSR) management. X.509 is a public key infrastructure standard that SSL adheres to for its key and certificate managment. Since we are wanting to create a new X.509 certificate, this is what we want. 
* -x509: This option specifies that we want to make a self-signed certificate file instead of generating a certificate request. 
* -nodes: This option tells OpenSSL that we do not wish to secure our key file with a passphrase. Having a password protected key file would get in the way of Apache starting automatically as we would have to enter the password every time the service restarts. 
* -days 365: This specifies that the certificate we are creating will be valid for one year. 
* -newkey rsa:2048: This option will create the certificate request and a new private key at the same time. This is necessary since we didn't create a private key in advance. The rsa:2048 tells OpenSSL to generate an RSA key that is 2048 bits long. 
* -keyout: This parameter names the output file for the private key file that is being created. 
* -out: This option names the output file for the certificate that we are generating. 
  ​    
Activer le module ssl dans apache :
​	
	sudo a2enmod ssl

Il faut décommenter et renseigner le chemin de la clé et du certificat ssl :

	sudo nano /etc/apache2/sites-available/default-ssl.conf

	A self-signed (snakeoil) certificate can be created by installing
	      #   the ssl-cert package. See
	      #   /usr/share/doc/apache2/README.Debian.gz for more info.
	      # If both key and certificate are stored in the same file, only the
	      #   SSLCertificateFile directive is needed.
	      #SSLCertificateFile     /etc/ssl/certs/ssl-cert-snakeoil.pem
	      #SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
	      SSLCertificateFile     /etc/apache2/ssl/apache.crt
	      SSLCertificateKeyFile /etc/apache2/ssl/apache.key

Puis activer default-ssl.conf :
​	
	sudo a2ensite default-ssl.conf

Vérifier le fichier /etc/apache2/ports.conf que le serveur apache2 écoute bien le port 443 comme ci dessous :

	# If you just change the port or add more ports here, you will likely also
	# have to change the VirtualHost statement in
	# /etc/apache2/sites-enabled/000-default.conf
	
		Listen 80
	
	<IfModule ssl_module>
		Listen 443
	</IfModule>
	
	<IfModule mod_gnutls.c>
		Listen 443
	</IfModule>

Redémarrer Apache2

	sudo service apache2 restart

### 3°) Installer Radicale : Ici pour Debian 9 Stretch (Version 1.1.1 dans les dépôts) :

	sudo apt-get update && sudo apt-get install -y radicale
Commande qui permet de mettre à jour les dépôts puis d'installer radicale  
ou
​	
	sudo apt update && sudo apt install -y radicale
...qui utilise apt, plus récent. ça ne change pas grand chose cependant à l'installation...culture générale.
​	
Le ftp debian <http://ftp.debian.org/debian/pool/main/r/radicale/> permet de trouver toutes les versions de Radicale. La version 2 est dans les dépots testing pour le moment normalement. La commande apt-get ou apt installe la version stable. Il est recommandé d'installer via les dépôts, cela installera en plus toutes les dépendances necessaires à son fonctionnement comme le paquet "python3-radicale" par exemple.

### 4°) Pour activer le lancement de Radicale en temps que service, il faut éditer le fichier /etc/default/radicale

	sudo nano /etc/default/radicale

et décommenter la 4ème ligne afin que le fichier ressemble à cela :

	# Defaults for radicale calendar server (sourced from /etc/init.d/radicale)

	# Uncomment to start radicale on system startup
	ENABLE_RADICALE=yes
	#
	# Options for radicale on startup
	# Note that most options can be specified in /etc/radicale/config
	RADICALE_OPTS="--daemon"
	#
	# Make initscript verbose, override system default
	# (Note: This is ONLY about the initscript!)
	VERBOSE=yes

### 5°) Configurer RADICALE :
#### a°) Se rendre dans le dossier /etc/radicale afin d'y éditer et créer les fichiers :

	cd /etc/radicale/

Les fichiers présents dans ce dossier sont par défaut :
​	
	config logging

#### b°) Editer le fichier « config » :

	sudo nano config

Ci-dessous, mon fichier config qui ne correspond peut être pas à votre utilisation mais qui vous permettra de comprendre les options minimales nécessaires au fonctionnement. Des options restent à explorer et à tester…..

EXEMPLE de fichier modifié :  /etc/radicale/config. Il est commenté afin de comprendre ce que vous faites.

```
# -*- mode: conf -*-
# vim:ft=cfg
# Config file for Radicale - A simple calendar server
#
# Place it into /etc/radicale/config (global)
# or ~/.config/radicale/config (user)
#
# The current values are the default ones
[server]

# CalDAV server hostnames separated by a comma
# IPv4 syntax: address:port
# IPv6 syntax: [address]:port
# For example: 0.0.0.0:9999, [::]:9999
# IPv6 adresses are configured to only allow IPv6 connections
hosts = 0.0.0.0:5232
# Port de connexion. Ici laissé par défaut. Retirer le #

# Daemon flag
daemon = True
# Retirer le # afin que le paramètre soit pris en compte.

# File storing the PID in daemon mode
#pid =

# SSL flag, enable HTTPS protocol
ssl = True
# J'ai choisi la sécurité. Retirer le #. Il faut donc renseigner l'url du certificat et de sa clé créé précédemment. Retirer le # et éditer le chemin. voir ci dessous.

# SSL certificate path
certificate = /etc/apache2/ssl/apache.crt

# SSL private key
key = /etc/apache2/ssl/apache.key

# SSL Protocol used. See python's ssl module for available values
#protocol = PROTOCOL_SSLv23

# Ciphers available. See python's ssl module for available ciphers
	#ciphers =

# Reverse DNS to resolve client address in logs
	#dns_lookup = True

# Root URL of Radicale (starting and ending with a slash)
	base_prefix = /
# Retirer le # et laisser ainsi. l'URL de connexion aux collections sera du type décrit ci dessous (cf configuration des clients)

# Possibility to allow URLs cleaned by a HTTP server, without the base_prefix
	#can_skip_base_prefix = False

# Message displayed in the client when a password is needed
	realm = Radicale - Un mot de passe est necessaire
# On peux personnaliser le message qui demande un mot de passe. Retirer le #

[encoding]

# Encoding for responding requests
request = utf-8
# Retirer les #. Peut être utile sous LINUX

# Encoding for storing local collections
stock = utf-8
# Retirer les #. Peut être utile sous LINUX

[well-known]

# Path where /.well-known/caldav/ is redirected
#caldav = '/%(user)s/caldav/'

# Path where /.well-known/carddav/ is redirected
#carddav = '/%(user)s/carddav/'
# Je ne l'utilise pas.

[auth]

# Authentication method
# Value: None | htpasswd | IMAP | LDAP | PAM | courier | http | remote_user | custom
type = htpasswd
# La connexion aux agendas se fait par le duo Login / Passe. Je n'utilise pas les autres options. A explorer donc.

# Custom authentication handler
#custom_handler =

# Htpasswd filename
htpasswd_filename = /etc/radicale/users
# Chemin du fichier « users » dans lequel sont créé les utilisateurs avec leur passe. La ligne en dessous doit spécifier la méthode de cryptage. Ici sha1 (cf plus bas, création des utilisateurs)

# Htpasswd encryption method
# Value: plain | sha1 | ssha | crypt
htpasswd_encryption = sha1

# LDAP server URL, with protocol and port
#ldap_url = ldap://localhost:389/

# LDAP base path
#ldap_base = ou=users,dc=example,dc=com

# LDAP login attribute
#ldap_attribute = uid

# LDAP filter string
# placed as X in a query of the form (&(...)X)
# example: (objectCategory=Person)(objectClass=User)(memberOf=cn=calenderusers,ou=users,dc=example,dc=org)
# leave empty if no additional filter is needed

#ldap_filter =

# LDAP dn for initial login, used if LDAP server does not allow anonymous searches
# Leave empty if searches are anonymous
#ldap_binddn =

# LDAP password for initial login, used with ldap_binddn
#ldap_password =

# LDAP scope of the search
#ldap_scope = OneLevel

# IMAP Configuration
#imap_hostname = localhost
#imap_port = 143
#imap_ssl = False

# PAM group user should be member of
#pam_group_membership =

# Path to the Courier Authdaemon socket
#courier_socket =

# HTTP authentication request URL endpoint
#http_url =
# POST parameter to use for username
#http_user_parameter =
# POST parameter to use for password
#http_password_parameter =

[git]

# Git default options
#committer = Radicale <radicale@example.com>

[rights]

# Rights backend
# Value: None | authenticated | owner_only | owner_write | from_file | custom
type = from_file
# Permet de gérer les droits d'accès des utilisateurs aux différentes collections. Il est associé à un fichier « rights » dont on renseigne le chemin et qu'il faudra éditer (voir plus bas). Voici la signification des autres options disponibles : None : accès en lecture/écriture de tous les utilisateurs à toutes les collections. owner_only : accès en lecture/écriture à toutes ses propres collections et aucun accès aux autres collections. owner_write : accès en lecture/écriture à toutes ses propres collections et les utilisateurs authentifiés ont un accès en lecture aux collections des autres. from_file : Accès en lecture/écriture à toutes ses propres collections et l'accès aux autres collections est paramétré au cas par cas dans un fichier de configuration. 

# Custom rights handler
#custom_handler =

# File for rights management from_file
file = /etc/radicale/rights

[storage]

# Storage backend
# -------
# WARNING: ONLY "filesystem" IS DOCUMENTED AND TESTED,
#          OTHER BACKENDS ARE NOT READY FOR PRODUCTION.
# -------
# Value: filesystem | multifilesystem | database | custom
type = filesystem
# Les données sont stockées dans des fichiers. Il faut décommenter cette ligne mais ne pas modifier le type.

# Custom storage handler
#custom_handler =

# Folder for storing local collections, created if not present
#filesystem_folder = /var/lib/radicale/collections
filesystem_folder = /etc/radicale/collections
#  Ici, le chemin du dossier contenant les collections. J'ai préféré tout mettre dans le dossier /etc/radicale. ATTENTION, les collections et l'ensemble des dossiers et fichiers qu'il contient doivent appartenir à l'utilisateur et au groupe « radicale »

# Database URL for SQLAlchemy
# dialect+driver://user:password@host/dbname[?key=value..]
# For example: sqlite:///var/db/radicale.db, postgresql://user:password@localhost/radicale
# See http://docs.sqlalchemy.org/en/rel_0_8/core/engines.html#sqlalchemy.create_engine
#database_url =

[logging]

# Logging configuration file
# If no config is given, simple information is printed on the standard output
# For more information about the syntax of the configuration file, see:
# http://docs.python.org/library/logging.config.html
config = /etc/radicale/logging
# Set the default logging level to debug
debug = False
# Store all environment variables (including those set in the shell)
full_environment = False
# Pour suivre le fonctionnement de radicale dans les logs.

[headers]

# Additional HTTP headers
#Access-Control-Allow-Origin = *
# non exploré, je ne sais pas à quoi cela sert.

```

#### c°) Créer le dossier qui recevra les collections :

J'ai choisi dans ma configuration de placer ce dossier dans /etc/radicale. Il faut donc le créer à cet endroit et il doit être la propriété de l'utilisateur:groupe **radicale**.

	sudo mkdir /etc/radicale/collections

	sudo chown -R radicale:radicale collections/

#### d°) Créer les utilisateurs :

##### 1°) Installer le paquet apache2-utils s'il n'est pas déjà présent :

	sudo apt-get update && sudo apt-get install apache2-utils

##### 2°) Utiliser l'outil « htpasswd » inclus dans le paquet précédemment installé pour créer l'utilisateur :

	sudo htpasswd -cs /etc/radicale/users Utilisateur

Les options de la commande :  
	-c : Permet de créer le fichier « users »  
	-s : Pour créer un cryptage SHA1 (cf fichier de configuration et vérifier le paramètre [auth])

#### e°) Contenu final des dossiers /etc/radicale et /etc/radicale/collections

Au final nous avons ceci dans le dossier radicale :

```
user@server:~$ ls -al /etc/radicale/
total 32  
drwxr-xr-x  3 root     root     4096 août  22 14:05 .
drwxr-xr-x 66 root     root     4096 août  16 11:46 ..  
drwxr-xr-x  5 radicale radicale 4096 août  23 18:51 collections  
-rw-r--r--  1 root     root     4511 août  22 14:45 config  
-rw-r--r--  1 root     root     1142 mai   16 14:29 logging  
-rwxr-xr-x  1 radicale radicale  795 août  24 19:05 rights  
-rw-r--r--  1 root     root       88 août  19 11:54 users  
```

et ceci dans le dossier « collections » :

```
user@server:~$ ls -al /etc/radicale/collections/
total 32  
drwxr-xr-x 5 radicale radicale 4096 août  23 18:51 .
drwxr-xr-x 3 root     root     4096 août  22 14:05 ..
drwxr-xr-x 3 radicale radicale 4096 août  23 19:08 User1
-rw-r----- 1 radicale radicale   54 août  23 18:51 User1.props
-rw-r----- 1 radicale radicale   20 août  20 18:31 .props
drwxr-xr-x 3 radicale radicale 4096 août  24 19:37 User2
-rw-r----- 1 radicale radicale  534 août  21 18:52 User2.props
drwxr-x--- 2 radicale radicale 4096 août  21 01:11 .well-known
```

### 6:) Se connecter et créer collections :

La création d'un agenda se fait en tapant son URL. Ça fonctionne bien dans Thunderbird/Lightning et même sur Firefox (j'ai pas testé les autres navigateurs et dans « Calendrier » sur Os X et iOS). Le login et mot de passe du propriétaire de l'agenda doit être renseigné.

La syntaxe est la suivante.

>https://serveur:port/utilisateur/nomdelagenda.ics/ (slash de fin obligatoire)

Le port est 5232 par défaut
Utilisateur : Créé précédemment avec htpasswd. (noms stockées dans users)
agenda.ics : Nom de l'agenda à créer ou à connecter. Ne pas oublier le slash à la fin.

A la première connexion radicale demandera le login et le mot de passe de l'utilisateur propriétaire.

Dans le dossier « collection » on trouvera alors un dossier correspondant au nom de l'utilisateur et un fichier nomutilisateur.props au même niveau. Le dossier contiendra les différentes collections de cet utilisateur, les fichiers .ics et .vcf…..

### 7°) Edition du fichier rights :

Une fois les collections créées, nous pouvons attribuer les droits d'accès aux utilisateurs à l'aide du fichier /etc/radicale/rights. Il faut tout d'abord créer le fichier qui n'existe pas par défaut. On peux l'éditer et le créer à l'enregistrement comme ceci. (Installer nano, sur un système Debian d'origine il n'est pas installé ou utiliser vim )

	sudo nano /etc/radicale/rights

Et changer son propriétaire :

	sudo chown radicale:radicale /etc/radicale/rights

Voilà par exemple la syntaxe de ce fichier à adapter selon vos besoins: 

	[owner can write its collections]
	user: .+
	collection: ^%(login)s.*$
	permission: rw
	
	[User1 can read User2/agenda1.ics]
	user: ^User1$
	collection: ^User2/agenda1.ics$
	permission: r
	
	[User1 can read and write User2/agenda2.ics]
	user: ^User1$
	collection: ^User2/agenda2.ics$
	permission: rw
	
	[User2 can read User1/agenda1.ics]
	user: ^User2$
	collection: ^User1/agenda1.ics$
	permission: r



Si besoin voir [http://radicale.org/user_documentation/#idfrom-file]() pour plus d'exemples.

Il ne faut pas oublier de relancer le serveur radicale après la fin de la configuration :
​	
	sudo service radicale restart


### 8°) Configuration des clients :

#### a°) Lightning (extension) sur Thunderbird :

« Lightning » était auparavant une extension de Thunderbird. Depuis la version 38, il est intégré par défaut. Les agendas dans « lightning » doivent être importés un par un. Lightning ne découvre pas de lui même les collections (voir « calendrier » sur iOS ou OS X) C'est justement pour cette raison que je créé mes agendas avec ce logiciel.

Aller sur :
Fichier / Nouveau / Agenda.

Fenêtre « Créer un nouvel agenda.  →  Sur le réseau

Format : icalendar (ICS)

Emplacement : https://serveur:5232/User1/agenda1.ics/ (si l'agenda n'existe pas, il sera créé automatiquement)

Par la suite Lightning demandera l'utilisateur et le mot de passe : Saisir User1 / passeuser1 si User1 en est le propriétaire. Saisir User2 / passeuser2 si User2 a des droits en lecture, ou lecture/écriture défini dans le fichier « rights »

Si vous essayez de créer un évènement sur un agenda en lecture seule, Lightning vous renverra une fenêtre d'erreur.

#### b°) Calendrier sur OS X 10.10 et sur Iphone iOS 8 :

##### IMPORTER LES AGENDAS D'UN UTILISATEUR :

Sur OS X : Ouvrir « Calendrier »  
PUIS  Calendrier /Ajouter un compte…  
PUIS  « Ajouter un compte Caldav... »

Fenêtre suivante :  
Type de compte : Manuel  
Nom d'utilisateur : User1  
Mot de passe : passeUser1  
Adresse du serveur : https://serveur:5232/User1/

Le / de la fin est obligatoire.

« Calendrier » trouvera de lui même les collections .ics correspondantes au propriétaire.

##### IMPORTER LES AGENDAS PARTAGES QUI APPARTIENNENT A UN AUTRE UTILISATEUR :

Type de compte : Manuel  
Nom d'utilisateur : User1  
Mot de passe : passeUser1  
Adresse du serveur : https://serveur:5232/User2/agenda2.ics/
Ci dessus, dans le fichier « rights » Le User1 a par exemple des droits en lecture sur le agenda Calendar2.ics appartenant à User2. (Ne pas oublier d'éditer le fichier (cf plus haut)

Il faut créer un compte par agenda partagé.

#### c°) DAVdroid sur Android :
L'import des comptes propriétaire est très facile et DAVdroid découvre de lui même et classe les collections en fonction de leur extension .ics (agenda) ou .vcf (contacts)

DAVdroid est disponible sur Play Store a un petit prix ou gratuitement dans les dépôts de F-DROID.

Choisir : « Login with URL and user name »

« https:// » est déjà présélectionné : Continuer en entrant : serveur:5232/User1/

DAVdroid proposera d'ajouter ou non ( on peux choisir les collections à ajouter) les agendas et contacts de User1.

NB : DAVdroid permet d'importer des agendas partagés mais il faut pour cela creer autant de comptes que d' agendas partagés en entrant l'URL jusqu'au bout, à savoir  
https://https://serveur:5232/User2/agenda2-partage.ics/  
Pour le login et le mot de passe, on entre biensûr celui de User1.
DavDroid propose toute la liste des agendas y compris ceux de User1, il faut alors unquement cliquer sur l'agenda partagé. Attention, le Fichier Right doit être correctement renseigné.


NB : Je n'ai pas parlé dans ce tutoriel des fichiers .vcf, l'import est identique aux fichiers .ics. On peut même sans soucis copier un fichier déjà existant (j'ai récupéré le mien à partir de owncloud qui ne me convenait plus) et le copier dans le répertoire de l'utilisateur dans /etc/radicale/collections/User1 par exemple. (ATTENTION ce fichier doit appartenir à radicale faire un sudo chown radicale:radicale /chemin/chemin.vcf au besoin et vérifier avec un ls -al que les droits d'accès sont bien en lecture et écriture pour le user.

#### d°) Les Contacts :
DAVdroid les trouvera de lui même (avec la configuration plus haut) et créera une section Contacts. Les groupes déja créé sont importés. 

Contact sur OS X (fonctionnement similaire sur iOS dans les Préférences), faire  Contacts/Ajouter un compte…./Autre compte contacts….
Renseigner l'utilisateur, le mot de passe puis comme ceci : https://serveur:5232/User1/contacts.vcf/
L'import des groupes de contacts n'est pas respecté.


## B°) CONFIGURATION AVANCEE (Fonctionnement de Radicale derrière Apache) :

J'ai fait le choix dans cette configuration qu'Apache gère l'authentification des utilisateurs (plus puissant avec Apache) et Radicale les droits d'accès aux bibliothèques (plus puissant avec Radicale). Il faut directement ici utiliser le port http ou le port https pour se connecter à Radicale. Radicale devient donc ici une librairie de Apache. Cette configuration a permis de plus de régler des soucis de crach de Radicale rendant non fonctionnel l'accès à mes bibliothèques.

### 1°) Installer et activer mod_WSGI afin de pouvoir utiliser Radicale qui est une Application PYTHON dans Apache :

	sudo apt-get install libapache2-mod-wsgi

Dans ma configuration, une fois installé, le module WSGI a été automatiquement activé. (Cf /etc/apache2/mods-enabled).


### 2°) Configuration de WSGI :

#### a°) /var/www/html/radicale.wsgi :
Afin de pouvoir accéder à Radicale depuis Apache, il faut tout d'abord créer un fichier  « radicale.wsgi » (fichier qui peut bien sur prendre n'importe quel nom) que l'on place dans  /var/www/html (racine des sites web par défaut). Le fichier contient les lignes suivantes :

	import radicale  
	radicale.log.start()
	application = radicale.Application()

#### b°) Le Fichier Virtual Host /etc/apache2/sites-available/default-ssl.conf :
Il est ensuite nécessaire de modifier le fichier default-ssl.conf dans /etc/apache2/sites-available

```
<VirtualHost _default_:443>
		ServerAdmin webmaster@localhost

		DocumentRoot /var/www/html

		ServerName URL SERVEUR
#Indiquer ici l'URL de votre serveur ou le domaine

    WSGIDaemonProcess radicale user=radicale group=radicale threads=1
    WSGIScriptAlias / /var/www/html/radicale.wsgi
#Chemin vers votre fichier radicale.wsgi créé précédemment. On peux indiquer ici un « Alias » afin de personnaliser le lien vers Radicale : https://nomduserveur/alias par exemple (Nécessite d'éditer également le fichier de conf de radicale)


    <Directory /var/www/html/>
    #Ici le repertoire du fichier du script.
    
        WSGIProcessGroup radicale
        WSGIApplicationGroup %{GLOBAL}
       
        AuthType Basic
        AuthName "Radicale vous demande de vous authentifier."
        AuthBasicProvider file
        AuthUserFile /etc/radicale/users
        Require valid-user
	#J'ai choisi de confier l'authentification de mes utilisateurs à Apache. Il faut donc renseigner le chemin du fichier users. On peut placer ce fichier n'importe ou et si c'est plus simple pour vous, copiez le dans /etc/apache2…
	
	AllowOverride None
        Order allow,deny
        allow from all

        #RewriteEngine Off
        #RewriteCond %{REMOTE_USER}%{PATH_INFO} !^([^/]+/)\1
        #RewriteRule .* - [Forbidden]
    </Directory>    
```

Il faut ensuite activer ce Virtual Host par la commande :

	sudo a2ensite radicale.conf

Ou tout simplement redémarrer Apache ou le serveur :

	sudo service apache2 restart 

### 3°) Modifier le fichier /etc/radicale/config :

Le bon réflexe tout d'abord, c'est de faire une copie de ce fichier :

	sudo cp /etc/radicale/config /etc/radicale/config.ORIG

Puis on modifie ce fichier ainsi :

```
# -*- mode: conf -*-
# vim:ft=cfg

# Config file for Radicale - A simple calendar server
#
# Place it into /etc/radicale/config (global)
# or ~/.config/radicale/config (user)
#
# The current values are the default ones


[server]

# CalDAV server hostnames separated by a comma
# IPv4 syntax: address:port
# IPv6 syntax: [address]:port
# For example: 0.0.0.0:9999, [::]:9999
# IPv6 adresses are configured to only allow IPv6 connections
#hosts = 0.0.0.0:5232
#Ce n'est plus le port de radicale qui est utilisé mais http ou https soit 80 ou 443 (par defaut). On commente donc la ligne avec #

# Daemon flag
daemon = False
# Radicale ne doit plus se comporter comme un Daemon. C'est Apache qui l'appelle comme une librairie.

# File storing the PID in daemon mode
#pid =

# SSL flag, enable HTTPS protocol
#ssl = True

# SSL certificate path
#certificate = /etc/apache2/ssl/server.crt

# SSL private key
#key = /etc/apache2/ssl/server.key
# Cf trois lignes ci dessus concernant SSL. C'est maintenant Apache qui gère la sécurité TLS. Il faut donc indiquer à Apache le lien vers ces fichiers dans son fichier de configuration.

# SSL Protocol used. See python's ssl module for available values
#protocol = PROTOCOL_SSLv23

# Ciphers available. See python's ssl module for available ciphers
#ciphers =

# Reverse DNS to resolve client address in logs
#dns_lookup = True

# Root URL of Radicale (starting and ending with a slash)
base_prefix = /
# A editer si vous désirez un préfixe. Il faut mettre le même préfixe dans le fichier default-ssl.conf

# Possibility to allow URLs cleaned by a HTTP server, without the base_prefix
#can_skip_base_prefix = False

# Message displayed in the client when a password is needed
#realm = Radicale - Un mot de passe est necessaire
# Inutile puisque ce n'est plus Radicale qui gère l'authentification des utilisateurs. On met donc un #

[encoding]

# Encoding for responding requests
#request = utf-8

# Encoding for storing local collections
#stock = utf-8


[well-known]

# Path where /.well-known/caldav/ is redirected
#caldav = '/%(user)s/caldav/'

# Path where /.well-known/carddav/ is redirected
#carddav = '/%(user)s/carddav/'


[auth]

# Authentication method
# Value: None | htpasswd | IMAP | LDAP | PAM | courier | http | remote_user | custom
#type = htpasswd
# C'est Apache qui gère maintenant l'authentification des utilisateurs. Le lien vers le fichier users est à renseigner dans le fichier VIRTUALHOST (default-ssl.conf chez moi par defaut)

# Custom authentication handler
#custom_handler =

# Htpasswd filename
#htpasswd_filename = /etc/radicale/users
# On commente ici aussi donc.

# Htpasswd encryption method
# Value: plain | sha1 | ssha | crypt
#htpasswd_encryption = sha1
# Vérifier qu'ici aussi, on est un #

# LDAP server URL, with protocol and port
#ldap_url = ldap://localhost:389/

# LDAP base path
#ldap_base = ou=users,dc=example,dc=com

# LDAP login attribute
#ldap_attribute = uid

# LDAP filter string
# placed as X in a query of the form (&(...)X)
# example: (objectCategory=Person)(objectClass=User)(memberOf=cn=calenderusers,ou=users,dc=example,dc=org)
# leave empty if no additional filter is needed
#ldap_filter =

# LDAP dn for initial login, used if LDAP server does not allow anonymous searches
# Leave empty if searches are anonymous
#ldap_binddn =

# LDAP password for initial login, used with ldap_binddn
#ldap_password =

# LDAP scope of the search
#ldap_scope = OneLevel

# IMAP Configuration
#imap_hostname = localhost
#imap_port = 143
#imap_ssl = False

# PAM group user should be member of
#pam_group_membership =

# Path to the Courier Authdaemon socket
#courier_socket =

# HTTP authentication request URL endpoint
#http_url =
# POST parameter to use for username
#http_user_parameter =
# POST parameter to use for password
#http_password_parameter =


[git]

# Git default options
#committer = Radicale <radicale@example.com>


[rights]
# Par contre, on laisse à Radicale la gestion des droits des utilisateurs (Partages et/ou accès en Lecture/Ecriture)

# Rights backend
# Value: None | authenticated | owner_only | owner_write | from_file | custom
type = from_file

# Custom rights handler
#custom_handler =

# File for rights management from_file
#file = ~/.config/radicale/rights
file = /etc/radicale/rights
# Chemin vers le fichier des droits

[storage]

# Storage backend
# -------
# WARNING: ONLY "filesystem" IS DOCUMENTED AND TESTED,
#          OTHER BACKENDS ARE NOT READY FOR PRODUCTION.
# -------
# Value: filesystem | multifilesystem | database | custom
#type = filesystem

# Custom storage handler
#custom_handler =

# Folder for storing local collections, created if not present
#filesystem_folder = /var/lib/radicale/collections
filesystem_folder = /etc/radicale/collections
# Radicale gère aussi le stockage des données des collections. Il faut donc renseigner le lien vers le dossier des collections. Par défaut, Radicale utilise des fichiers pour stocker les données (c'est d'ailleurs recommandé de laisser ce paramètre par défaut)

# Database URL for SQLAlchemy
# dialect+driver://user:password@host/dbname[?key=value..]
# For example: sqlite:///var/db/radicale.db, postgresql://user:password@localhost/radicale
# See http://docs.sqlalchemy.org/en/rel_0_8/core/engines.html#sqlalchemy.create_engine
#database_url =


[logging]

# Logging configuration file
# If no config is given, simple information is printed on the standard output
# For more information about the syntax of the configuration file, see:
# http://docs.python.org/library/logging.config.html
#config = /etc/radicale/logging
# Set the default logging level to debug
#debug = False
# Store all environment variables (including those set in the shell)
#full_environment = False

[headers]

# Additional HTTP headers
#Access-Control-Allow-Origin = *
```

### 4°) Editer le fichier /etc/default/radicale :

Le fichier */etc/default/radicale* doit être édité en commentant les lignes ci-dessous. Radicale ne fonctionne plus comme un daemon et ne doit pas être demmarré au demmarrage du serveur, c'est Apache qui 'appellera quand c'est necessaire :

```
# Defaults for radicale calendar server (sourced from /etc/init.d/radicale)

# Uncomment to start radicale on system startup
#ENABLE_RADICALE=yes

# Options for radicale on startup
# Note that most options can be specified in /etc/radicale/config
#RADICALE_OPTS="--daemon"

# Make initscript verbose, override system default
# (Note: This is ONLY about the initscript!)
VERBOSE=yes
```

### 5°) Fermer le port 5232, inutile maintenant :

Afficher les règles de Iptables en numérotant les lignes :
​	
	sudo iptables -L --line-numbers
Supprimer la ligne correspondant à l'ouverture du port 5232 : « N° » correspond au numéro de la ligne à retirer.

​	

	sudo iptables -D INPUT « N° »
Reconfigurer iptables-persistent (à installer si ce n'est pas déjà fait) pour rendre les règles persistantes au démarrage du serveur.

​	

	sudo dpkg-reconfigure -y iptables-persistent


### 6°) Configurer les comptes sur les clients :

La configuration des clients est identique que précédemment. MAIS, il ne faut pas préciser le port puisque l'on passe maintenant par les ports de Apache. Si vous n'avez pas modifié les ports par défaut de Apache, 80 pour http et 443 pour https, vous accédez donc aux collections de l'utilisateur 1 par :  
https://nomduserveur/utilisateur1  
ou si vous avez précisé un préfixe dans la configuration :  
https://nomduserveur/prefixe/utilisateur1 


D'autres informations ici :
Site officiel : [http://radicale.org/user_documentation/]()

[http://www.rezine.org/documentation/auto-hebergement/services/agenda-contact/serveurs-cal-card/#index3h1]()
[http://it-tuto.com/2015/05/radicale-carddavcaldavinstaller-son-propre-service-de-synchronisation-contacts-calendriers/]()
[http://jonathantutorial.blogspot.fr/2014/10/how-to-set-up-radicale.html]()

**AUTEURS :**  

_Sébastien MILHERES_ avec l'importante participation de _PLOC et de Julien PECCOUD_ qui m'ont largement aidé sur la mise en place de RADICALE sur mon serveur Debian 9.